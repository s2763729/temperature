package nl.utwente.di.temperature;

public class Converter {
    public double convert(String celsius) {
        double cels = Double.parseDouble(celsius);
        double fahrenheit = cels * 1.8 + 32;
        return fahrenheit;
    }
}
